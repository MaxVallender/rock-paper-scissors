Rock-Paper-Scissors
===================

by Max Vallender

An basic implementation of the logic, from the hand game Rock–paper–scissors.

It produces output like the following:

		"Player A wins 31 of 100 games"
		"Player B wins 37 of 100 games"
		"Tie: 32 of 100 games"

There is also a very primitiv implementation of a human player. With a little modification (Main.java, line 58), the game has the posibility to played by a (or two) human players.

## Get code

Clone repository with git:

		$ git clone https://bitbucket.org/MaxVallender/Rock-Paper-Scissors.git

or download repository under:

		https://bitbucket.org/MaxVallender/Rock-Paper-Scissors/downloads/

Build and run project (required Java and Maven):

		$ mvn clean install

		$ java -jar target/RockPaperScissors-...

