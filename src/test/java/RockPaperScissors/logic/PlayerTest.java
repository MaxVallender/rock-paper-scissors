/**
 * 
 */
package RockPaperScissors.logic;

import RockPaperScissors.logic.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;

/**
 * @author max
 *
 */
class PlayerTest {
	static Player pAiPaper;
	static Player pAiRandom;
	static Player pHuman;

	@BeforeAll
	static void init() {
		pAiPaper = new PlayerAiPaper("Ai Paper");
		pAiRandom = new PlayerAiRandom("AI Random");
		pHuman = new PlayerHuman("Human");
	}

	/**
	 * Test method for {@link main.java.RockPaperScissors.logic.PlayerAiPaper#nextHand()}.
	 */
	@Test
	void testPlayerAiPaper_NextHand() {
		assertEquals(Hand.Paper, pAiPaper.nextHand());
		
	}

	/**
	 * Test method for {@link main.java.RockPaperScissors.logic.PlayerAiRandom#nextHand()}.
	 */
	@Test
	void testPlayerAiRandom_NextHand() {
		for (int i=0; i<100; i++)
			assertThat(pAiRandom.nextHand(), anyOf(is(Hand.Scissors), is(Hand.Paper), is(Hand.Rock)));
	}

	/**
	 * Test method for {@link main.java.RockPaperScissors.logic.PlayerHuman#nextHand()}.
	 */
	@Test
	@Disabled
	void testPlayerHuman_NextHand() {
		assertThat(pHuman.nextHand(), anyOf(is(Hand.Scissors), is(Hand.Paper), is(Hand.Rock)));
	}
}
