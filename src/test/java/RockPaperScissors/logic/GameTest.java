package RockPaperScissors.logic;

import RockPaperScissors.logic.*;

import java.util.stream.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class GameTest {
	static Game g;

	@BeforeAll
	static void init() {
		Player playerA, playerB;
		playerA = new PlayerAiRandom("Player A");
		playerB = new PlayerAiPaper("Player B");
//		playerA = new PlayerHuman("Human A");
		g = new Game(playerA, playerB);
	}

	/**
	 * Tests all three possible cases of updating the score.
	 * 
	 * Saves score of next winner and sum temporary. Execute updateScore(...).
	 * Checks if winner score goes +1 and sum goes +1. 
	 */
	@Test
	void testScoreSystem() {
		int temp, tempSum;

		// win: a
		temp = g.scoreArray()[0];
		tempSum = IntStream.of(g.scoreArray()).sum();
		g.updateScore(Hand.Scissors, Hand.Paper);
		assertEquals(temp + 1, g.scoreArray()[0]);
		assertEquals(tempSum + 1, IntStream.of(g.scoreArray()).sum());

		// win: b
		temp = g.scoreArray()[1];
		tempSum = IntStream.of(g.scoreArray()).sum();
		g.updateScore(Hand.Paper, Hand.Scissors);
		assertEquals(temp + 1, g.scoreArray()[1]);
		assertEquals(tempSum + 1, IntStream.of(g.scoreArray()).sum());
		
		// tie
		temp = g.scoreArray()[2];
		tempSum = IntStream.of(g.scoreArray()).sum();
		g.updateScore(Hand.Paper, Hand.Paper);
		assertEquals(temp + 1, g.scoreArray()[2]);
		assertEquals(tempSum + 1, IntStream.of(g.scoreArray()).sum());
	}

	/**
	 * Tests only if play() does a gameplay.
	 */
	@Test
	void testPlay() {
		int tempAll;
		
		// a + b + tie
		tempAll = IntStream.of(g.scoreArray()).sum();
		g.play();
		assertEquals(tempAll + 1, IntStream.of(g.scoreArray()).sum());
	}
}
