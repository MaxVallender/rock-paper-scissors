package RockPaperScissors.logic;

import RockPaperScissors.logic.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class HandTest {

	@Test
	void init() {
		Hand h = Hand.Paper;
	}
	
	/**
	 * Tests isBeating(...) with all possible combination of Hand-symbols. 
	 */
	@Test
	final void testIsBeating() {
		assertEquals(true, (Hand.Scissors).isBeating(Hand.Paper));
		assertEquals(false, (Hand.Paper).isBeating(Hand.Scissors));
		
		assertEquals(true, (Hand.Paper).isBeating(Hand.Rock));
		assertEquals(false, (Hand.Rock).isBeating(Hand.Paper));
		
		assertEquals(true, (Hand.Rock).isBeating(Hand.Scissors));
		assertEquals(false, (Hand.Scissors).isBeating(Hand.Rock));
		
		assertEquals(false, (Hand.Scissors).isBeating(Hand.Scissors));
		assertEquals(false, (Hand.Paper).isBeating(Hand.Paper));
		assertEquals(false, (Hand.Rock).isBeating(Hand.Rock));
	}
}