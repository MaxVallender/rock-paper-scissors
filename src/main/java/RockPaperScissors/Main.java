package RockPaperScissors;

import RockPaperScissors.logic.*;

import java.util.Random;

/**
 * Implementation of the game Rock-Paper-Scissors.
 * 
 * (In the main you will find a short simple solution. It produces exact the 
 * wanted output.)
 * 
 * After that there is the real solution, which uses different classes and does 
 * a real gameplay. By default there are two AI-Players. One chooses randomly a 
 * Hand-symbol. The other always choose Paper. (A human player could be 
 * activated, by modifying.)
 * 
 * @author Max Vallender
 * 
 */
public class Main {
	public static void main(String[] args) {

// Simple Solution ;oP
		int a=0, b=0, tie=0;
		Random r = new Random();
		
		// Scissor, Paper, Rock
		/*
		 * S > P
		 * P > R
		 * R > S
		 * 
		 * A: P = 1
		 * B: random = 0 1 2
		 */
		for (int i=1; i <= 100; i++) {
			switch (r.nextInt(3)) {
				case 0: a++; break;
				case 2: b++; break;
				default: tie++;
			}
		}
		
		System.out.println("Player A wins " + a + " of 100 games");
		System.out.println("Player B wins " + b + " of 100 games");
		System.out.println("Tie: " + tie + " of 100 games");
		
//		"Player A wins 31 of 100 games"
//		"Player B wins 37 of 100 games"
//		"Tie: 32 of 100 games"

// Real Solution

		// Initialize new Game, with Player A, Player B
		Player playerA = new PlayerAiRandom("Player A");
		Player playerB = new PlayerAiPaper("Player B");
//		playerA = new PlayerHuman("Human A");
		Game g = new Game(playerA, playerB);
		
		// Do 100 rounds of gameplay
		for (int i=1; i <= 100; i++)
			g.play();
		
		// Print result
		System.out.println(g.scoreString());
	}
}
