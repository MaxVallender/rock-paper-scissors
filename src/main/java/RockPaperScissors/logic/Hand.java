package RockPaperScissors.logic;

/**
 * Enumeration Hand is for an easy handling with the three Hand-symbols. They 
 * are Scissors (0), Paper (1) and Rock (2). It also has a method which Hand is 
 * beating the given Hand.
 * 
 * @author Max Vallender
 */
public enum Hand {
	Scissors, Paper, Rock;

	/**
	 * Compares this Hand to a given Hand.
	 * 
	 * S beats P	(0 beats 1),
	 * P beats R	(1 beats 2),
	 * R beats S	(2 beats 0),
	 * 
	 * @param h is a given Hand
	 * @return true/false, depending if this Hand beats the given Hand h. Could 
	 * be true by winning. Could be false by loosing or a tie. 
	 */
	public boolean isBeating(Hand h) {
		return ((this.ordinal() + 1 == h.ordinal()) || 
				(this.ordinal() == 2 && h.ordinal() == 0));
	}
}