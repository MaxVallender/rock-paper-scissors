package RockPaperScissors.logic;

/**
 * Implementation of abstract Player. In form of an AI, which produces only the 
 * Hand.Paper-symbol.
 * 
 * @author Max Vallender
 */
public class PlayerAiPaper extends Player {

	public PlayerAiPaper(String name) {
		this.name = name;
	}
	
	Hand nextHand() {
		return Hand.Paper;
	}
}