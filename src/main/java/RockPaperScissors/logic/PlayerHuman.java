package RockPaperScissors.logic;

import java.io.IOException;

/**
 * Implements abstract Player class. It lets a human take part in the game, by 
 * asking what Hand-symbol should be used next.
 * 
 * @author Max Vallender
 */
public class PlayerHuman extends Player {

	public PlayerHuman(String name) {
		this.name = name;
	}
	
	/**
	 * The method nextHand() asks the human about what Hand-symbol should be 
	 * played next. If the input doesn't match it is always Rock.
	 *  
	 * The method works, but didn't work in a nice way yet. 
	 * 
	 * @return Hand
	 */
	@Deprecated
	Hand nextHand() {
		char c = 's';
		
		System.out.println(getName() + ": (S)cissor, (P)aper, (R)ock ?");
		try {
			c = (char)System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}

		switch(Character.toLowerCase(c)){
			case 's': return Hand.Scissors;
			case 'p': return Hand.Paper;
			default: return Hand.Rock;
		}
	}
}