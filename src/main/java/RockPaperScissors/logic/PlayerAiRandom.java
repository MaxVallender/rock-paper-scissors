package RockPaperScissors.logic;

import java.util.Random;

/**
 * Implementation of abstract Player. In form of an AI, which produces random 
 * Hand-symbols.
 *  
 * @author Max Vallender
 */
public class PlayerAiRandom extends Player {
	private Random r = new Random();

	public PlayerAiRandom(String name) {
		this.name = name;
	}
	
	Hand nextHand() {
		return Hand.values()[r.nextInt(3)];
	}
}