package RockPaperScissors.logic;

/**
 * Controller of the game Rock-Paper-Scissors. Handles the Players and the 
 * score.
 * 
 * @author Max Vallender
 */
public class Game {
	private Player playerA, playerB;
	private int a, b, tie;

	/**
	 * Initializes the Game, with two Players. 
	 *  
	 * @param nameA is set as name of Player A.
	 * @param nameB is set as name of Player B.
	 */
	public Game(Player playerA, Player playerB) {
		this.playerA = playerA;
		this.playerB = playerB;
		
		a = b = tie = 0;
	}
	
	/** 
	 * Update the score, from the current gaming situation.
	 * 
	 * @param handA is the current Hand from Player A
	 * @param handB is the current Hand from Player B  
	 */
	void updateScore(Hand handA, Hand handB) {
		if (handA.isBeating(handB)) {
			a++;
		} else {
			if (handB.isBeating(handA)) {
				b++;
			} else
				tie++;
		}
	}

	/**
	 * Initiates the next round in the gameplay.
	 */
	public void play() {
		updateScore(playerA.nextHand(), playerB.nextHand());
	}

	/**
	 * @return int[] - score of the game, as triple of the points from Player a, Player b and the ties.  
	 */
	public int[] scoreArray() {
		return new int[] {a, b, tie};
	}
	
	/**
	 * @return String - score of the game
	 */
	public String scoreString() {
		return (playerA.getName() + " wins " + a + " of " + (a + b + tie) + " games\n" + 
				playerB.getName() + " wins " + b + " of " + (a + b + tie) + " games\n" + 
				"Tie: " + tie + " of " + (a + b + tie) + " games");
	}
}
