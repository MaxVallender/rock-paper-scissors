package RockPaperScissors.logic;

/**
 * Abstract class for Player, with its name. Has an abstract method nextHand() 
 * which must be implemented, for the next decision of which Hand-symbol he 
 * shows. 
 * 
 * @author Max Vallender
 */
public abstract class Player {
	protected String name;
	
	/**
	 * @return String with name of the Player
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Genereates a Hand-symbol.
	 * 
	 * @return Hand
	 */
	abstract Hand nextHand();
}